﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMananger : MonoBehaviour {
    public GameObject pausePanel;
    public GameObject controlPanel;
    private GameObject selectedCube;

    public Toggle tiltToggle;
    public Toggle gyroToggle;

    private bool isPaused;
    private bool isControls;

	// Use this for initialization
	void Start () {
        tiltToggle = tiltToggle.GetComponent<Toggle>();
        gyroToggle = gyroToggle.GetComponent<Toggle>();
	}

    /** Landscape and Potrait scales **/
	
	// Update is called once per frame
	void Update () {
        // Checks if game is paused
		if (isPaused)
        {
            this.PauseGame(true);
        } else
        {
            this.PauseGame(false);
        }

        if(isControls)
        {
            this.controlPanel.SetActive(true);
        } else
        {
            this.controlPanel.SetActive(false);
        }
	}

    // Pauses game
    void PauseGame(bool state)
    {
        if (state)
        {
            Time.timeScale = 0.0f;
            this.pausePanel.SetActive(true);
        } else
        {
            Time.timeScale = 1.0f;
            this.pausePanel.SetActive(false);
        }
    }

    // Switches pause state
    public void SwitchPause()
    {
        this.isPaused = !this.isPaused;
    }

    // Exits game
    public void ExitGame()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #endif

        Application.Quit();
    }

    // Resets camera angle
    public void ResetCamera()
    {
        Camera.main.transform.rotation = Quaternion.Euler(0, Camera.main.transform.rotation.y, 0);
    }

    // Switches tilt toggle state
    public void SwitchTitleToggle()
    {
        if (tiltToggle.isOn == true)
        {
            MobileController.isTilt = true;
        } else
        {
            MobileController.isTilt = false;
        }
    }

    // Switches gyro toggle state
    public void SwitchGyroToggle()
    {
        if (gyroToggle.isOn == true)
        {
            MobileController.isGyro = true;
        } else
        {
            MobileController.isGyro = false;
        }
    }

    // Destroys selected cube
    public void DestroyCube()
    {
        if (MobileController.selectedCube)
        {
            Destroy(MobileController.selectedCube);
        }
    }

    // Deselects cube
    public void DeselectCube()
    {
        MobileController.ResetColor();
        MobileController.selectedCube = null;
    }

    // Switch control menu
    public void SwitchControls()
    {
        this.isControls = !this.isControls;
    }
}
