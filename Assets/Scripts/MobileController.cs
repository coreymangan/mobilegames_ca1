﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileController : MonoBehaviour {
    private float orthoZoomSpeed = 0.5f;
    private float perspZoomSpeed = 0.5f;
    private float maxZoom = 15.0f;
    private float tiltSpeed = 12.5f;
    private float verSpeed = 1.5f;
    private float horSpeed = 0.5f;
    private float rotateSpeed = 0.5f;
    private float scaleSpeed = 0.2f;
    public static bool isTilt;
    public static bool isGyro;
    public static GameObject selectedCube;

    // Use this for initialization
    void Start () {
        Screen.SetResolution((int)Screen.width, (int)Screen.height, true);
    }
	
	// Update is called once per frame
	void Update () {
        MultiTouch();
        Tilt();
        Gyro();
        }

    // Handles multi touch
    void MultiTouch()
    {
        // Drag or Pan Camera
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);

            Ray screenRay = Camera.main.ScreenPointToRay(touch.position);
            RaycastHit hit;
            if (Physics.Raycast(screenRay, out hit))
            {
                selectedCube = hit.collider.gameObject;
                // If single touch then change color, If moving then move object
                if (touch.phase == TouchPhase.Began)
                {
                    ChangeColor(hit);
                } else if (touch.phase == TouchPhase.Moved)
                {
                    MoveObject(hit);
                }
            } else
            {
                // Pan camera if moving not on object
                if (touch.phase == TouchPhase.Moved)
                {
                    PanCamera();
                }
            }
        }

        // Zoom/Rotate Camera or Scale Object
        if (Input.touchCount == 2)
        {
            if(selectedCube)
            {
                ScaleObject();
            } else
            {
                ZoomOrRotate();
            }

            //RotateTwoFingers();
        }

        // Rotate Camera
        if (Input.touchCount == 3)
        {
            RotateCamera();
        }

        // Move Forwards
        if (Input.touchCount == 4)
        {
            transform.transform.Translate(0, 0, verSpeed * Time.deltaTime);
        }

        // Move Backwards
        if (Input.touchCount == 5)
        {
            transform.transform.Translate(0, 0, -verSpeed * Time.deltaTime);
        }
    }

    // Changes color of object
    void ChangeColor(RaycastHit hit)
    {
        hit.collider.GetComponent<Renderer>().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
    }

    // Movies position of object
    void MoveObject(RaycastHit hit)
    {
        hit.collider.transform.position = new Vector3(hit.point.x, hit.point.y, hit.collider.transform.position.z);
    }

    // Accelerometer Camera
    void Tilt()
    {
        if (isTilt)
        {
            Vector3 dir = Vector3.zero;
            dir.x = Input.acceleration.x;
            dir.z = Input.acceleration.y + 0.7f;

            if (dir.sqrMagnitude > 1)
            {
                dir.Normalize();
            }

            dir *= Time.deltaTime;

            transform.Translate(dir * tiltSpeed);
        }
    }

    // GyroScope Camera
    void Gyro()
    {
        if (isGyro)
        {
            Gyroscope gyro = Input.gyro;
            gyro.enabled = true;

            transform.Rotate(-Input.gyro.rotationRateUnbiased.x, -Input.gyro.rotationRateUnbiased.y, Input.gyro.rotationRateUnbiased.z);
        }
    }

    // Zooms Camera (old zoom)
    void ZoomCamera()
    {
        Touch t1 = Input.GetTouch(0);
        Touch t2 = Input.GetTouch(1);

        if (t1.phase == TouchPhase.Moved && t2.phase == TouchPhase.Moved)
        {
            Vector2 t1PrevPos = t1.position - t1.deltaPosition;
            Vector2 t2PrevPos = t2.position - t2.deltaPosition;

            float prevDeltaMag = (t1PrevPos - t2PrevPos).magnitude;
            float deltaMag = (t1.position - t2.position).magnitude;

            float deltaMagDiff = prevDeltaMag - deltaMag;

            // Zoom
            if (Camera.main.orthographic)
             {
                Camera.main.orthographicSize += deltaMagDiff * orthoZoomSpeed;
                 Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize, 0.1f);
             } else
             {
                 Camera.main.fieldOfView += deltaMagDiff * perspZoomSpeed;
                 Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView, 10.0f, 150.0f);
             }
        }
    }

    // Scales object
    void ScaleObject()
    {
        Touch t1 = Input.GetTouch(0);
        Touch t2 = Input.GetTouch(1);

        if (t1.phase == TouchPhase.Moved && t2.phase == TouchPhase.Moved)
        {
            Vector2 t1PrevPos = t1.position - t1.deltaPosition;
            Vector2 t2PrevPos = t2.position - t2.deltaPosition;

            float prevDeltaMag = (t1PrevPos - t2PrevPos).magnitude;
            float deltaMag = (t1.position - t2.position).magnitude;

            float deltaMagDiff = prevDeltaMag - deltaMag;

            // Expand
            if (deltaMagDiff <= 0 && selectedCube.transform.localScale.x < 5.0f)
            {
                selectedCube.transform.localScale += new Vector3(scaleSpeed, scaleSpeed, scaleSpeed);
            }

            // Shrink
            if (deltaMagDiff >= 0 && selectedCube.transform.localScale.x > 1.0f)
            {
                selectedCube.transform.localScale -= new Vector3(scaleSpeed, scaleSpeed, scaleSpeed);
            }
        }
    }

    // Pans Camera
    void PanCamera()
    {
        Touch touch = Input.GetTouch(0);
        if (touch.phase == TouchPhase.Moved)
        {

            float t1PosX = Mathf.Abs(touch.deltaPosition.x);
            float t1PosY = Mathf.Abs(touch.deltaPosition.y);

            Vector2 t1PrevPos = touch.position - touch.deltaPosition;
            Vector2 t1Distnce = t1PrevPos - touch.position;

            if (t1PosX > t1PosY)
            {
                transform.Translate(t1Distnce.x * horSpeed * Time.deltaTime, 0, 0);
            }
            else
            {
                transform.Translate(0, t1Distnce.y * verSpeed * Time.deltaTime, 0);
            }
        }
    }

    // Rotates Camera (old rotate)
    void RotateCamera()
    {
        Touch t1 = Input.GetTouch(0);
        Touch t2 = Input.GetTouch(1);

        if (t1.phase == TouchPhase.Moved && t2.phase == TouchPhase.Moved)
        {

            float t1PosX = Mathf.Abs(t1.deltaPosition.x);
            float t1PosY = Mathf.Abs(t1.deltaPosition.y);

            Vector2 t1PrevPos = t1.position - t1.deltaPosition;
            Vector2 t1Distnce = t1PrevPos - t1.position;

            if (t1PosX > t1PosY)
            {
                transform.Rotate(0, t1Distnce.x * rotateSpeed, 0);
            } else
            {
                transform.Rotate(t1Distnce.y * rotateSpeed, 0, 0);
            }
        }
    }

    // Resets color back to white when unselected
    public static void ResetColor()
    {
        selectedCube.GetComponent<Renderer>().material.color = Color.white;
    }

    // Rotates camera (old rotate)
    void RotateTwoFingers()
    {
        Touch t1 = Input.GetTouch(0);
        Touch t2 = Input.GetTouch(1);

        float prevYDif = t1.deltaPosition.y - t2.deltaPosition.y;
        float prevXDif = t1.deltaPosition.x - t2.deltaPosition.x;
        float prevAngle = prevYDif / prevXDif;

        float yDif = t1.position.y - t2.position.y;
        float xDif = t1.position.x - t2.position.x;
        float angle = yDif / xDif;

        float t1PosX = Mathf.Abs(t1.deltaPosition.x);
        float t1PosY = Mathf.Abs(t1.deltaPosition.y);

        if (prevAngle != 0 && angle != 0)
        {
            float rotateDiff = angle - prevAngle;
            Debug.Log(rotateDiff);
            if (rotateDiff > 1)
            {
                Debug.Log("rotate right");
                transform.Rotate(0, 0, rotateSpeed);
            } else if (rotateDiff < 1)
            {
                Debug.Log("rotate left");
                transform.Rotate(0, 0, -rotateSpeed);
            }
        }
    }


    // Code to zoom or rotate camera with 2 fingers

    const float pinchTurnRatio = Mathf.PI / 2;
    const float minTurnAngle = 0.5f;
    const float pinchRatio = 1;
    const float minPinchDistance = 2.5f;
    const float panRatio = 1;
    const float minPanDistance = 0;
    private float turnAngleDelta;
    private float turnAngle;
    private float pinchDistanceDelta;
    private float pinchDistance;

    private void ZoomOrRotate()
    {
        Debug.Log("here 1");
        pinchDistance = pinchDistanceDelta = 0;
        turnAngle = turnAngleDelta = 0;

        // if two fingers are touching the screen at the same time ...
            Touch touch1 = Input.touches[0];
            Touch touch2 = Input.touches[1];

            // ... if at least one of them moved ...
            if (touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved)
            {
            Debug.Log("here 2");
                // ... check the delta distance between them ...
                pinchDistance = Vector2.Distance(touch1.position, touch2.position);
                float prevDistance = Vector2.Distance(touch1.position - touch1.deltaPosition,
                                                      touch2.position - touch2.deltaPosition);
                pinchDistanceDelta = pinchDistance - prevDistance;

                // ... if it's greater than a minimum threshold, it's a pinch!
                if (Mathf.Abs(pinchDistanceDelta) > minPinchDistance)
                {
                    pinchDistanceDelta *= pinchRatio;
                // Zoom
                if (Camera.main.orthographic)
                {
                    Camera.main.orthographicSize += pinchDistanceDelta * orthoZoomSpeed;
                    Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize, 0.1f);
                }
                else
                {
                    Camera.main.fieldOfView += pinchDistanceDelta * perspZoomSpeed;
                    Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView, 10.0f, 150.0f);
                }
            }
                else
                {
                    pinchDistance = pinchDistanceDelta = 0;
                }

                // ... or check the delta angle between them ...
                turnAngle = Angle(touch1.position, touch2.position);
                float prevTurn = Angle(touch1.position - touch1.deltaPosition,
                                       touch2.position - touch2.deltaPosition);
                turnAngleDelta = Mathf.DeltaAngle(prevTurn, turnAngle);

                // ... if it's greater than a minimum threshold, it's a turn!
                if (Mathf.Abs(turnAngleDelta) > minTurnAngle)
                {
                    turnAngleDelta *= pinchTurnRatio;
                    transform.Rotate(0, 0, turnAngleDelta);
            }
                else
                {
                    turnAngle = turnAngleDelta = 0;
                }
            }
    }

    // Gets Angle
    private float Angle(Vector2 pos1, Vector2 pos2)
    {
        Vector2 from = pos2 - pos1;
        Vector2 to = new Vector2(1, 0);

        float result = Vector2.Angle(from, to);
        Vector3 cross = Vector3.Cross(from, to);

        if (cross.z > 0)
        {
            result = 360f - result;
        }

        return result;
    }
}
